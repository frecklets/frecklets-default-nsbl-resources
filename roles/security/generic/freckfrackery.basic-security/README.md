## freckfrackery.basic-security
<!-- This file was generated by Ansigenome. Do not edit this file directly but
     instead have a look at the files in the ./meta/ directory. -->

[![Platforms](http://img.shields.io/badge/platforms-debian%20/%20el%20/%20macosx%20/%20ubuntu-lightgrey.svg?style=flat)](#)


This role gathers a few tasks commonly executed in order to harden a machine that is exposed (or not) to the internet.



### Role variables

List of default variables available in the inventory:

```YAML
---
# defaults file for basic-security

# all of those variables are optional, depending on whether they are set to a non-empty value
# or not determines whether certain tasks will be executed

basic_security_root_pw: ''                            # the password for the root user
basic_security_user_name: ''                          # the username for the admin user
basic_security_user_pw: ''                            # the password for the admin user
basic_security_user_shell: '/bin/bash'                # the shell for the admin user
basic_security_user_public_keys: []                   # a list of public ssh keys for the admin user
basic_security_packages: []                           # a list of packages to install
basic_security_use_mosh: false                        # whether to setup and configure mosh
basic_security_ssh_port: 22                           # the port to use for the ssh daemon
basic_security_mosh_from_port: 60000                  # the min port to use with mosh
basic_security_mosh_to_port: 60010                    # the max port to use with mosh
basic_security_disable_ssh_password_auth: false       # disable ssh logins using password auth
basic_security_disable_ssh_root_access: false         # disable ssh login for the root user
basic_security_enable_passwordless_sudo: false        # enable passwordless sudo for the admin user
basic_security_autoupdate_enabled: false              # enable automatic updates on this machine
basic_security_enable_ufw: false                      # whether to enable the ufw firewall
basic_security_tcp_ports: []                          # open tcp ports (in combination with the ufw firewall)
basic_security_udp_ports: []                          # open udp ports (in combination with the ufw firewall)
basic_security_enable_fail2ban: false                 # whether to install and enable fail2ban
basic_security_force_ssh_config: false                # disable sanity ssh config check
```

List of internal variables used by the role:

    basic_security_packages
    __new_user__
    __use_mosh__
### Usage


#### Details
TBD

#### Example playbook

```YAML
- hosts: all
  roles:
    - freckfrackery.basic-security
```


### Authors and license

`freckfrackery.basic-security` role was written by:

- [Markus Binsteiner](https://freckles.io) | [e-mail](mailto:makkus@frkl.io) | [Twitter](https://twitter.com/__frkl__)

License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

***
This role is part of the [freckles](https://freckles.io) project.

README.md generated by [Ansigenome](https://github.com/nickjj/ansigenome/).
