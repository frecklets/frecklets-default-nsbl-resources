# -*- coding: utf-8 -*-
from ansible.module_utils.six import string_types


class FilterModule(object):

    def filters(self):

        return {
            "filter_non_ansible_role": self.filter_non_ansible_role,
            "filter_ansible_role": self.filter_ansible_role,
            "pretty_print_packages": self.pretty_print_packages
        }

    def filter_ansible_role(self, package_list):

        result = []
        for p in package_list:
            if p["pkg_mgr"] == "ansible_role":
                result.append(p)
        return result

    def filter_non_ansible_role(self, package_list):

        result = []
        for p in package_list:
            if p["pkg_mgr"] != "ansible_role":
                result.append(p)
        return result

    def pretty_print_packages(self, package_list):

        result = []

        for p in package_list:
            if isinstance(p, string_types):
                result.append(p)
            else:
                try:
                    name = p.get("name", None)
                    if name:
                        result.append(name)
                except (Exception):
                    pass

        return result
